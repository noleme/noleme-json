# Noleme JSON

_Last updated for v0.9_

This library is meant as a collection of JSON utility functions and most precisely as a specification for JSON third-party dependencies usage throughout all Noleme libraries.
This is basically the place where the version requirements for Jackson (or other) are decided, and developments relying on JSON as a vehicle for data should use whichever
implementation is specified by this project.

_Note: This library is considered as "in beta" and as such significant API changes may occur without prior warning._

## I. Installation

Add the following in your `pom.xml`:

```xml
<dependency>
    <groupId>com.noleme</groupId>
    <artifactId>noleme-json</artifactId>
    <version>0.9</version>
</dependency>
```

## II. Notes on Structure and Design

_TODO_

## III. Usage

_TODO_

## IV. Dev Installation

### A. Pre-requisites

This project will require you to have the following:

* Git (versioning)
* Maven (dependency resolving, publishing and packaging)

### B. Setup

_TODO_
